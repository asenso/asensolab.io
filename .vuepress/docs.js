module.exports = [
    {
        title: "Getting Started",
        collapsable: false,
        children: [
            "installation",
            "configuration"
        ]
    },
    {
        title: "Basics",
        collapsable: false,
        children: prefix("basics", [
            "middleware",
            "permission",
            "routing"
        ])
    },
    {
        title: "Features",
        collapsable: false,
        children: prefix('features', [
            "breadcrumb",
            "modules",
            "widget",
            "views"
        ])
    }
];

function prefix(prefix, children) {
    return children.map(child => `${prefix}/${child}`);
}