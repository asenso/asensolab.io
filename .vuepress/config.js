module.exports = {
    title: 'Asenso',
    description: 'Asenso eCommerce Platform',
    dest: 'public',
    head: [
        [
            "link",
            {
                href: "https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,700,200italic,400italic,700italic",
                rel: "stylesheet",
                type: "text/css"
            }
        ]
    ],
    themeConfig: {
        sidebarDepth: 2,
        search: false,
        smoothScroll: true,
        nav: [
            { text: 'Home', link: '/' },
            { text: 'Docs', link: '/docs/' }
        ],
        sidebar: {
            '/docs/': require('./docs')
        }
    }
}
