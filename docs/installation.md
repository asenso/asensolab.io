# Installation

- [Installation](#installation)
    - [Server Requirements](#server-requirements)
    - [Installing Asenso](#installing-asenso)
- [Configuration](#configuration)

<a name="installation"></a>
## Installation

<a name="server-requirements"></a>
### Server Requirements

The Asenso eCommerce Platform has a few system requirements. Below is the list of what
you need to make sure your server meets the following requirements:

* PHP &gt;= 7.1.3
* OpenSSL PHP Extension
* PDO PHP Extension
* GD Library (Image Processing)
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Ctype PHP Extension
* JSON PHP Extension
* Curl PHP Extension 

<a name="installing-asenso"></a>
### Installing Asenso

Asenso utilizes [Composer](https://getcomposer.or) to manage its dependencies. So, before using Asenso, make sure you have Composer installed on your machine.

#### Via Asenso Installer

:x: Under Construction :construction: :x:

#### Via Composer Create-Project



#### Serving Your Application

<a name="configuration"></a>
### Configuration

#### Application Key